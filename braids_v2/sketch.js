let rowHeight;
let stringSpacing;
let stringThickness;
let margin;
let bgColor;
let numStrings;
let crossingProbability;
let positiveProbability;
let crossingAngle;
let controlYFactor;
let spacerGap;
let generatorsInLastRow;
let colors;
let timer;
let context;
let contextDim;
let x;
let strandsX;
let strings;
let crossings;
let testValue = 1; //remove probability of crossing for testing purposes

let marginControl;

function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight);

  contextDim = width/2;
  context = createGraphics(contextDim, contextDim);
  background(225);

  rectMode(CENTER);
  // textAlign(CENTER);
  textSize(16);

  // length of each strand before crossing - tightness or looseness of the braid
  rowHeight = 33;
	stringSpacing = 20;
	stringThickness = 8;
	bgColor = color(127);
	// numStrings = 1 + floor((context.width-stringThickness)/stringSpacing);
	numStrings = 3;
  // where the braids are placed on the canvas. 0 == left, context.width == right
  margin = (context.width - (numStrings-1)*stringSpacing)/2; // centers the strands
	crossingProbability = 0.87;
	positiveProbability = 0.5;
  // the size of the strand "shadow" that is left behind after a crossing
	spacerGap = 10;
  x = 0;

  crossingAngle = 47*PI/180; // degrees to radians 
  // controls how "curvy" the crossings are. .1 = angular, 1 = very curvy
	controlYFactor = (1 - stringSpacing/rowHeight*tan(crossingAngle));

  colors = initialColors();

  // initialize generatorsInLastRow - an array which records which crossing generators (left or right) appeared in the previous row.
  generatorsInLastRow = [];
  strandsX = [];
  strings = [];
  // crossings = [];
  for (let k = 0; k < numStrings-1; k++) {
    generatorsInLastRow.push(0);
    strandsX.push(0); 
  }

  for (let j = 0; j <= numStrings; j++){
    strings.push(0);
  }
      
  // timer = setInterval(onTimer,1000/10);
  let i = floor(height/rowHeight); // how many columns of strands to draw
  while (--i > -1) { // while i is greater than -1, decrement i
    fillRow(i*rowHeight);	
  }

  onTimer();

  noLoop();

}


function onTimer() {
  // scroll down
  // context.drawImage(displayCanvas, 0, 0, displayWidth, displayHeight-rowHeight, 0, rowHeight, displayWidth, displayHeight-rowHeight);
  // image(context, 0, 0, context.width, context.height-rowHeight, 0, 0, context.width, context.height-rowHeight);
  
  image(context, 0, 0);
  context.fill(bgColor); // why does this alter the background color instead of context.background?
  context.stroke(bgColor); // got rid of the borders around the context window
  context.rect(0,0,context.width,context.height);		

}


function fillRow(y0) { 
  let stringNumber = 0;
  let x0;
  let temp;
  let positiveSwitch;
  let doPositive;
  let prob = 0.5; //first crossing probability at 50%, rest will be set to desired crossingProbability set above.
  while (stringNumber < numStrings - 1) {
    // addresses each individual strand
    x0 = margin + stringNumber*stringSpacing;
    strandsX[stringNumber] = x0;
    
    // this is where all of the crossings take place. crossings only take place if prob is greater than random()
    if (testValue < prob) { // after .5 prob, prob is set to crossingProbability. so positive is any random() > prob??
      positiveSwitch = (random() < positiveProbability); // boolean. positiveSwitch is true if random() < positiveProbability
      // console.log(positiveSwitch);
      doPositive = (positiveSwitch && (generatorsInLastRow[stringNumber] != -1)) || // if not positive and gilr != -1 
              ((!positiveSwitch) && (generatorsInLastRow[stringNumber] == 1)); // if positive and gilr == 1
      if (doPositive) { // left to right
        // console.log("here");
        // just wanted to bring attention to the crossings
        drawCrossing(x0, y0, colors[stringNumber], colors[stringNumber+1], true);
        generatorsInLastRow[stringNumber] = 1; // current
        generatorsInLastRow[stringNumber+1] = 0; // next
      }
      else { // right to left
        // just wanted to bring attention to the crossings
        drawCrossing(x0, y0, colors[stringNumber], colors[stringNumber+1], false);
        generatorsInLastRow[stringNumber] = -1; // current
        generatorsInLastRow[stringNumber+1] = 0; // next
      }
      // permute colors
      temp = colors[stringNumber];
      colors[stringNumber] = colors[stringNumber+1];
      colors[stringNumber+1] = temp;
      // advance
      stringNumber += 2;
    }
    else { // if random() > prob 
      drawString(x0, y0, colors[stringNumber]);
      stringNumber += 1;
    }
    // console.log(strandsX);
  }
  if (stringNumber == numStrings - 1) {
    drawString(margin + stringNumber*stringSpacing, y0, colors[stringNumber]);
    strandsX[stringNumber] = margin + stringNumber*stringSpacing;
  }
  // after first crossing probability of 50%, remaining crossing probabilities set to desired amount.
  prob = crossingProbability;

  // console.log("stringNumber is " + stringNumber);
  // console.log("numStrings is " + numStrings);
  // console.log("gilr is " + generatorsInLastRow[stringNumber]);
}


function initialColors() {
  let i;
  let r,g,b;
  let param;
  let returnArray = [];
  // for (i = 0; i < numStrings; i++) {
  for (i = 0; i < numStrings; i++) {
    
    r = 64+floor(random()*192);
    g = 64+floor(random()*192);
    b = 64+floor(random()*192);
    
    returnArray.push("rgb("+r+","+g+","+b+")");
  }
  return returnArray;
}


function drawString(x0,y0,color) {
  context.stroke(color);
  context.strokeWeight(stringThickness);
  context.strokeCap(ROUND);
  context.beginShape();
  // context.position(x0,y0);
  context.line(x0,y0, x0,y0+rowHeight);
  context.stroke(color);
  endShape();
}


function drawCrossing(x0,y0,color1,color2,positive) {
				
  var midX = x0 + stringSpacing/2;
  var midY = y0 + rowHeight/2;
  
  context.strokeCap(ROUND);
  
  if (positive) {
    drawLine1();
    drawSpacer2();
    drawLine2();
  }
  else {
    drawLine2();
    drawSpacer1();
    drawLine1();
  }
  
  function drawLine1() {
    context.stroke(color1);
    context.strokeWeight(stringThickness);
    context.beginShape();
    // context.position(x0+stringSpacing,y0);
    context.bezier(x0+stringSpacing,y0, x0+stringSpacing, y0+rowHeight*controlYFactor, 
                x0, y0+rowHeight*(1-controlYFactor), 
                x0, y0+rowHeight);
    context.stroke(color1);
    context.endShape();
  }
  
  function drawSpacer1() {
    context.stroke(bgColor);
    context.strokeCap(SQUARE);
    context.strokeWeight(stringThickness + spacerGap*2);
    context.beginShape();
    // context.position(x0+stringSpacing,y0);
    context.bezier(x0+stringSpacing,y0, x0+stringSpacing, y0+rowHeight*controlYFactor, 
                x0, y0+rowHeight*(1-controlYFactor), 
                x0, y0+rowHeight);
    context.stroke(bgColor);
    context.endShape();
    context.stroke(0, 255, 0);
    
    // crossings
    // context.stroke(0, 255, 0);
    // let t = .5;
    // let bx1 = x0+stringSpacing;
    // let bx2 = x0+stringSpacing;
    // let bx3 = x0;
    // let bx4 = x0;
    // let by1 = y0;
    // let by2 = y0+rowHeight*controlYFactor;
    // let by3 = y0+rowHeight*(1-controlYFactor);
    // let by4 = y0+rowHeight;
    // let x = bezierPoint(bx1, bx2, bx3, bx4, t);
    // let y = bezierPoint(by1, by2, by3, by4, t);
    // context.ellipse(x, y, 3, 3);
    // crossings.push([x, y]);
  }
   
  
  function drawSpacer2() {
    context.stroke(bgColor);
    context.strokeCap(SQUARE);
    context.strokeWeight(stringThickness+2*spacerGap);
    context.beginShape();
    // context.position(x0,y0);
    context.bezier(x0,y0, x0, y0+rowHeight*controlYFactor, 
                x0+stringSpacing, y0+rowHeight*(1-controlYFactor), 
                x0+stringSpacing, y0+rowHeight);
    context.stroke(bgColor);
    context.endShape();

    // crossings
    // context.stroke(0, 0, 255);
    // let t = .5;
    // let bx1 = x0+stringSpacing;
    // let bx2 = x0+stringSpacing;
    // let bx3 = x0;
    // let bx4 = x0;
    // let by1 = y0;
    // let by2 = y0+rowHeight*controlYFactor;
    // let by3 = y0+rowHeight*(1-controlYFactor);
    // let by4 = y0+rowHeight;
    // let x = bezierPoint(bx1, bx2, bx3, bx4, t);
    // let y = bezierPoint(by1, by2, by3, by4, t);
    // context.ellipse(x, y, 3, 3);
    // crossings.push([x, y]);
  }


  function drawLine2() {
    context.stroke(color2);
    context.strokeWeight(stringThickness);
    context.beginShape();
    // context.position(x0,y0);
    context.bezier(x0,y0, x0, y0+rowHeight*controlYFactor, 
                x0+stringSpacing, y0+rowHeight*(1-controlYFactor), 
                x0+stringSpacing, y0+rowHeight);
    context.stroke(color2);
    context.endShape();
  }
}

function keyPressed(){

  if (keyCode == LEFT_ARROW) {
    margin -= 1;
  }
  if (keyCode == RIGHT_ARROW) {
    margin += 1;
  }

  if (keyCode == UP_ARROW) {
    numStrings += 1;
    margin = (context.width - (numStrings-1)*stringSpacing)/2; // centers the strands
    colors = initialColors();
    console.log(colors);
  }

  if (keyCode == DOWN_ARROW) {
    numStrings -= 1;
    margin = (context.width - (numStrings-1)*stringSpacing)/2; // centers the strands
    colors = initialColors();
    console.log(colors);
  }

  if (key == 'c' ) {
    console.log(strandsX);
    testValue = .4;
  }

  if (key == 'u' ) {
    console.log(strandsX);
    testValue = .6;
  }

  redraw();
}

function draw() {
  // put drawing code here
  // background(127);

  let i = floor(height/rowHeight); // how many columns of strands to draw
  while (--i > -1) { // while i is greater than -1, decrement i
    fillRow(i*rowHeight);	
  }

  onTimer();

  text('Use Up and Down arrow keys to change the number of strings', width - width / 2.1, height / 4 + 20)
  text('Use Left and Right arrow keys to change the margin', width - width / 2.1, height / 4 + 80)
  text('Use C key to add crossings', width - width / 2.1, height / 4 + 140)
  text('Use U key to remove crossings', width - width / 2.1, height / 4 + 200)

  
}