let osc;

let paths = [];
let vehicles = [];
let lineY;
let lineRadius;
let unlocked = true;
let oscSent = false;

function setup() {
	createCanvas(400, 400);
	// put setup code here
	
	for (i = 0; i < 4; i++) {

		let x = 50 + 100 * [i];

		paths[i] = new Path(x, 0, x, height);

		vehicles[i] = new Vehicle(x, 10)
		vehicles[i].vel.y = 1;
	}

	lineY = height / 2;
	lineRadius = 1;

	osc = new OSC();
	osc.open({ port: 9000 })

}

function sendOsc() {

	for(let i = 0; i < vehicles.length; i++){

		let lineX = 50 + 100 * [i];
		
		if (vehicles[0].checkIntersection(lineX) && unlocked) {
			unlocked = false;
			const message = new OSC.Message('/test0', i);
			osc.send(message);
			// console.log('sending: ' + intersection)
			
		} 

		if (vehicles[1].checkIntersection(lineX) && unlocked) {
			unlocked = false;
			const message = new OSC.Message('/test1', i);
			osc.send(message);
			// console.log('sending: ' + intersection)
			
		} 

		if (vehicles[2].checkIntersection(lineX) && unlocked) {
			unlocked = false;
			const message = new OSC.Message('/test2', i);
			osc.send(message);
			// console.log('sending: ' + intersection)
			
		}

		if (vehicles[3].checkIntersection(lineX) && unlocked) {
			unlocked = false;
			const message = new OSC.Message('/test3', i);
			osc.send(message);
			// console.log('sending: ' + intersection)
			
		}
		
		else if (!vehicles[0].checkIntersection(lineX) && !vehicles[1].checkIntersection(lineX) && !vehicles[2].checkIntersection(lineX) && !vehicles[3].checkIntersection(lineX) && !unlocked){
			unlocked = true;
		}
		// console.log('unlocked: ' + unlocked);
		// console.log('checkIntersection: ' + vehicles[i].checkIntersection(lineX));
	}

}

function draw() {
	// put drawing code here
	background(0);

	for (let i = 0; i < vehicles.length; i++) {

		// paths[i].end.x = mouseX;

		let force = vehicles[i].follow(paths[i]);
		vehicles[i].applyForce(force);

		vehicles[i].edges();
		vehicles[i].update();
		vehicles[i].show();

		paths[i].show();

	}

	sendOsc();

	stroke(255);
	strokeWeight(2)
	line(0, lineY, width, lineY);

}
